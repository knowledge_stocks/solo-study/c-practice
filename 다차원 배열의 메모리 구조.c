#include <stdio.h>

void main() {
    int arr[2][3] = {
        { 11, 22, 33 },
        { 44, 55, 66 }
    };

    printf("arr[1][2] = %d\n", arr[1][2]);
    printf("arr[0][5] = %d\n", arr[0][5]);
    printf("*((*arr)+5) = %d\n", *((*arr)+5));
    printf("%d\n", arr);
    printf("%d\n", arr + 1);
    printf("%d %d\n", *arr, arr[0]);
    printf("%d\n", (*arr)+1);
    printf("%d %d %d\n", *(arr+1), (*arr)+3, arr[1]);
    printf("%d %d %d\n", *(arr+1)+1, (*arr)+4, arr[1]+1);
}

// 실행 결과
// arr[1][2] = 66
// arr[0][5] = 66
// *(arr+5) = 66