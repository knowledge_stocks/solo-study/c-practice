#include <stdio.h>
#include <stdlib.h>

void main() {
    // int arr[3] = { 1, 3, 5 };

    // printf("%s\n", arr == &(arr[0]) ? "same" : "different");
    // printf("arr[0] ~ arr[2] = %d %d %d\n", arr[0], arr[1], arr[2]);
    // printf("*arr ~ *(arr+2) = %d %d %d\n", *arr, *(arr+1), *(arr+2));

    // // 실행 결과
    // // same
    // // arr[0] ~ arr[2] = 1 3 5
    // // *arr ~ *(arr+2) = 1 3 5

    int* arr = malloc(sizeof(int) * 3);
    arr[0] = 1;
    arr[1] = 3;
    arr[2] = 5;

    printf("%s\n", arr == &(arr[0]) ? "same" : "different");
    printf("arr[0] ~ arr[2] = %d %d %d\n", arr[0], arr[1], arr[2]);
    printf("*arr ~ *(arr+2) = %d %d %d\n", *arr, *(arr+1), *(arr+2));

    // 실행 결과
    // same
    // arr[0] ~ arr[2] = 1 3 5
    // *arr ~ *(arr+2) = 1 3 5
}