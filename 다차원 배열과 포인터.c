#include <stdio.h>

void main() {
    int arr[2][3] = {
        { 11, 22, 33 },
        { 44, 55, 66 }
    };

    printf("%d %d %d %d\n", arr, arr[0], *(arr), **(arr));
    printf("%d %d %d %d\n", arr[1], *(arr+1), **(arr+1), *(*(arr)+1));
}
// 실행 결과
// 6422280 6422280 6422280 11
// 6422292 6422292 44 22